from setuptools import setup, Command

class PyTest(Command):
    user_options = []
    def initialize_options(self):
        pass
    def finalize_options(self):
        pass
    def run(self):
        import subprocess
        errno = subprocess.call(['py.test', 'testing'])
        raise SystemExit(errno)


setup(
    name='jobtriggers',
    version='0.4.3',
    description='triggering jobs via fedmsg',
    author='Tim Flink',
    author_email='tflink@fedoraproject.org',
    license='GPLv2+',
    url='https://pagure.io/fedora-qa',
    packages=['jobtriggers'],
    entry_points={
        'console_scripts': ['jobrunner=jobtriggers.jobrunner:main'],
        'moksha.consumer': (
            'kojibuildcompletedjobtrigger = jobtriggers.koji_build_msg:KojiBuildCompletedJobTrigger',
            'kojitagchangedjobtrigger = jobtriggers.koji_tag_msg:KojiTagChangedJobTrigger',
            'composecompletedjobtrigger = jobtriggers.compose_complete_msg:ComposeCompletedJobTrigger',
        ),
    },
    include_package_data=True,
    install_requires=[
        'fedmsg',
        'requests',
    ],
    cmdclass={'test': PyTest}
)
