class RunnerException(Exception):
    pass


class TriggerError(Exception):
    pass


class TriggerMsgError(Exception):
    pass
