# The MongoTemplate is based on the Python 2.7's string library.
#  Sadly the string.Template can not be easily modified to this
#  extent without actually copying the actual code of substutite
#  and safe_substitute, because we need to add another group to
#  the pattern, and the methods dealing with it are encapsulated
#  in the two afore-mentioned.

import string


class _multimap:

    """Helper class for combining multiple mappings.

    Used by .{safe_,}substitute() to combine the mapping and keyword
    arguments.
    """

    def __init__(self, primary, secondary):
        self._primary = primary
        self._secondary = secondary

    def __getitem__(self, key):
        try:
            return self._primary[key]
        except KeyError:
            return self._secondary[key]


class MongoTemplate(string.Template):

    """Makes sure the mongo keywords ($and, $or, ...) don't get replaced.
    This will still replace ${and} ${or} though, and that is expected
    """
    __mongo_operators = [
        'exists',
        # comparison operators
        'gt', 'gte', 'in', 'lt', 'lte', 'ne', 'nin',
        # logical operators
        'and', 'nor', 'not', 'or',
        # element operators
        'type',
        # evaluation operators
        'mod', 'regex', 'options', 'text', 'where',
        # array operators
        'all', 'elemMatch', 'size',
        # comment operators
        'comment',
    ]

    pattern = r"""
    \$(?:
      (?P<escaped>\$) |   # Escape sequence of two delimiters
      (?P<mongo>(?:%s)\W) | # mongo keywords to 'ignore'
      (?P<named>[_a-z][_a-z0-9]*) |
      {(?P<braced>[_a-z][_a-z0-9]*)}   |   # delimiter and a braced identifier
      (?P<invalid>)              # Other ill-formed delimiter exprs
    )""" % '|'.join(__mongo_operators)

    def substitute(*args, **kws):
        if not args:
            raise TypeError("descriptor 'substitute' of 'Template' object "
                            "needs an argument")
        self, args = args[0], args[1:]  # allow the "self" keyword be passed
        if len(args) > 1:
            raise TypeError('Too many positional arguments')
        if not args:
            mapping = kws
        elif kws:
            mapping = _multimap(kws, args[0])
        else:
            mapping = args[0]
        # Helper function for .sub()

        def convert(mo):
            # Check the most common path first.
            named = mo.group('named') or mo.group('braced')
            if named is not None:
                val = mapping[named]
                # We use this idiom instead of str() because the latter will
                # fail if val is a Unicode containing non-ASCII characters.
                return '%s' % (val,)
            if mo.group('escaped') is not None:
                return self.delimiter
            if mo.group('mongo') is not None:
                return "$%s" % mo.group('mongo')
            if mo.group('invalid') is not None:
                self._invalid(mo)
            raise ValueError('Unrecognized XXX named group in pattern',
                             self.pattern)
        return self.pattern.sub(convert, self.template)

    def safe_substitute(*args, **kws):
        if not args:
            raise TypeError("descriptor 'safe_substitute' of 'Template' object "
                            "needs an argument")
        self, args = args[0], args[1:]  # allow the "self" keyword be passed
        if len(args) > 1:
            raise TypeError('Too many positional arguments')
        if not args:
            mapping = kws
        elif kws:
            mapping = _multimap(kws, args[0])
        else:
            mapping = args[0]
        # Helper function for .sub()

        def convert(mo):
            named = mo.group('named') or mo.group('braced')
            if named is not None:
                try:
                    # We use this idiom instead of str() because the latter
                    # will fail if val is a Unicode containing non-ASCII
                    return '%s' % (mapping[named],)
                except KeyError:
                    return mo.group()
            if mo.group('escaped') is not None:
                return self.delimiter
            if mo.group('mongo') is not None:
                return "$%s" % mo.group('mongo')
            if mo.group('invalid') is not None:
                return mo.group()
            raise ValueError('Unrecognized named group in pattern',
                             self.pattern)
        return self.pattern.sub(convert, self.template)
