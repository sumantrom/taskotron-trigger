import os
import re
import subprocess
import csv
import datetime
import shutil
import koji

import yaml

from jobtriggers import config
from jobtriggers import exceptions as exc


def log_job(logname, item, item_type, task, arch):
    now = datetime.datetime.now()

    with open(logname, 'a') as csvfile:
        writer = csv.writer(csvfile, delimiter=';')
        writer.writerow([now, item, item_type, task, arch])


def parse_yaml_from_file(filename):
    with open(filename, 'r') as datafile:
        try:
            return yaml.safe_load(datafile.read())
        except yaml.YAMLError, e:
            raise exc.TriggerError(e)


def _should_schedule(formula_file, item_type):
    formula = parse_yaml_from_file(formula_file)
    try:
        run = formula['scheduling']['run']
    except KeyError:
        run = True

    return run and item_type in formula['input']['args']


def get_distgit_branch(release):
    matches = re.findall(r'\.(fc\d{1,2})\.?', release)
    if not matches:
        raise exc.TriggerMsgError('Could not parse disttag from %s' % release)

    return matches[-1].replace('c', '')


def _get_koji_session():
    # for testability reasons...
    return koji.ClientSession("http://koji.fedoraproject.org/kojihub")


def get_distgit_branch_docker(build_id):
    session = _get_koji_session()
    tags = session.listTags(build_id)

    for tag in tags:
        try:
            matches = re.findall(r'^(f\d{1,2})-?.*$', tag['name'])
            if matches:
                return matches[-1]
        except KeyError:
            pass

    raise exc.TriggerMsgError('Could not get disttag for %d' % build_id)


def get_docker_registry_url(build_id):
    session = _get_koji_session()
    archives = session.listArchives(build_id, type='image')

    for archive in archives:
        try:
            urls = archive['extra']['docker']['repositories']
            for url in urls:
                if '@sha' not in url:
                    return url
        except KeyError:
            pass

    raise exc.TriggerMsgError('Could not get docker registry url from %d' % build_id)


def clone_repo(repo, branch='master', fallback=None):
    """Clones the repo & branch. If the branch can not be cloned, and fallback
    is set, it tries to clone fallback. If repo can not be cloned, raises exception.
    Returns full path to the cloned repo, and the cloned branch."""
    dirname = repo.split('/')[-1]
    if dirname.endswith('.git'):
        dirname = dirname[:-len(".git")]

    repo_dir = os.path.join(config.git_cache_dir, dirname)

    # for now, just delete the repo and make a new shallow copy
    if os.path.exists(repo_dir):
        shutil.rmtree(repo_dir)

    cmd = ['git', 'clone', '--branch', branch, '--depth', '1', repo, repo_dir]

    try:
        subprocess.check_output(cmd, stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as e:
        if fallback and branch != fallback:
            if 'Could not find remote branch' in e.output:
                return clone_repo(repo, branch=fallback)
        raise exc.TriggerError(e.output)

    return repo_dir, branch


def _discover_formulae(directory, recursive=False):
    """Returns a list of relative paths to 'runtask.yml' files
    in the directory.
    Search is recursive, as long as recursive is set to True.
    """
    formulae = []
    # remove trailing slashes
    directory = directory.strip().rstrip('/')

    for dirpath, dirs, files in os.walk(directory):
        if 'runtask.yml' in files:
            formula = os.path.join(dirpath, 'runtask.yml')
            # remove the base directory path
            formulae.append(formula[len(directory)+1:])
        if not recursive:
            break
        # remove hidden dirs
        # dirs needs to be updated in-place
        dirs[:] = [d for d in dirs if not d.startswith('.')]

    return formulae


def discover_tasks(directory, item_type, recursive=False):
    """Returns a list of relative paths to 'runtask.yml' files in the directory.
    Only formulae that are relevant for the item_type are returned.
    """
    formulae = _discover_formulae(directory, recursive)
    tasks = [f for f in formulae if _should_schedule(os.path.join(directory, f), item_type)]

    return tasks
