import re

import fedmsg
import fedmsg.encoding

from twisted.internet.task import LoopingCall

from . import config
from .jobtrigger import JobTrigger
from . import exceptions as exc

ITEM_TYPE = 'koji_tag'


def get_item(msg):
    koji_tag = msg['msg']['tag']
    instance = msg['msg']['instance']

    if not instance == 'primary':
        raise exc.TriggerMsgError('rejecting message not from primary koji instance (%s from %s)'
                                  % (koji_tag, instance))

    if not re.match('.*-updates(-testing)?-pending', koji_tag):
        raise exc.TriggerMsgError('rejecting message for not being an updates-(testing?)-pending',
                                  'tag (%s)' % koji_tag)

    m = re.match(r'^f\d\d-.*', koji_tag)
    if m is None:
        raise exc.TriggerMsgError('rejecting message for not starting with fedora version '
                                  '(e.g. f22) (%s)' % koji_tag)

    return koji_tag


class KojiTagChangedJobTrigger(JobTrigger):
    topic = "org.fedoraproject.prod.buildsys.tag"
    jsonify = False
    config_key = 'taskotron.kojitagchangedjobtrigger.enabled'

    def __init__(self, *args, **kw):
        super(KojiTagChangedJobTrigger, self).__init__(*args, **kw)

        self.queued_tags = set()
        self.queued_data = dict()
        # check for bodhi_to_koji_tags tasks queue every config.fuse_delay seconds
        lc = LoopingCall(self.delayed_consume)
        lc.start(config.fuse_delay)

    def delayed_consume(self):
        if self.queued_tags:
            try:
                for tag, data in self.queued_data.items():
                    self.do_trigger(data)
            finally:
                self.queued_data = dict()
                self.queued_tags = set()
        else:
            self.log.debug("Woke up, but there were no messages.")

    def consume(self, message):
        msg = fedmsg.encoding.loads(message.body)
        try:
            item = get_item(msg)
            if item in self.queued_tags:
                self.log.info("koji_tag %s already in the queue, skipping", item)
                return

            self.log.info('Adding koji_tag %s to the queue', item)

            data = {
                "_msg": msg,
                "message_type": "KojiTagChanged",
                "item": item,
                "item_type": ITEM_TYPE,
                "tag": item,
            }

            self.queued_tags.add(item)
            self.queued_data[item] = data
        except exc.TriggerMsgError, e:
            self.log.debug(e)
