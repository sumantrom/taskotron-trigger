from . import config  # This gets mocked by the test suite.
from .jobtrigger import JobTrigger
from . import exceptions as exc

MESSAGE_TYPE = 'DistGitCommit'
ITEM_TYPE = 'dist_git_commit'


class DistGitCommitJobTrigger(JobTrigger):
    topic = "org.fedoraproject.prod.git.receive"
    config_key = 'taskotron.distgitcommitjobtrigger.enabled'

    def process(self, msg):
        namespace = msg['msg']['commit']['namespace']
        repo = msg['msg']['commit']['repo']
        branch = msg['msg']['commit']['branch']
        rev = msg['msg']['commit']['rev']
        item = "%s/%s#%s" % (namespace, repo, rev)

        data = {
            "_msg": msg,
            "message_type": MESSAGE_TYPE,
            "item": item,
            "item_type": ITEM_TYPE,
            "namespace": namespace,
            "repo": repo,
            "branch": branch,
            "rev": rev,
        }
        self.do_trigger(data)

    def consume(self, message):
        msg = message.body
        try:
            self.process(msg)
        except exc.TriggerMsgError, e:
            self.log.debug(e)
            return
