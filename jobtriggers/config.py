import ConfigParser
import io

defaults = """
[buildbot]
url = http://localhost:8080/buildbot/change_hook

[trigger]
valid_arches = x86_64
koji_url = http://koji.fedoraproject.org/kojihub
datagrepper_url = https://apps.fedoraproject.org/datagrepper/raw
execdb_server = http://localhost:5003
runner_type = StreamRunner
job_logging = False
joblog_file = /var/log/taskotron-trigger/jobs.csv
fuse_delay = 1000 ; seconds
git_cache_dir = /var/lib/taskotron-trigger/cache
rules_template = ./conf/trigger_rules.yml.example

[koji_build_completed]
critpath_filepath = /var/lib/taskotron-trigger/critpath_whitelist
"""

config = ConfigParser.ConfigParser()
config.readfp(io.BytesIO(defaults))
config.read(['/etc/taskotron/trigger.cfg', './conf/trigger.cfg'])


buildbot_url = config.get('buildbot', 'url')

valid_arches = [e.strip() for e in config.get('trigger', 'valid_arches').split(',') if e]
koji_url = config.get('trigger', 'koji_url')
datagrepper_url = config.get('trigger', 'datagrepper_url')
execdb_server = config.get('trigger', 'execdb_server')
runner_type = config.get('trigger', 'runner_type')
job_logging = True if config.get('trigger', 'job_logging') == 'True' else False
joblog_file = config.get('trigger', 'joblog_file')
fuse_delay = int(config.get('trigger', 'fuse_delay'))  # seconds
git_cache_dir = config.get('trigger', 'git_cache_dir')
rules_template_file = config.get('trigger', 'rules_template')

critpath_filepath = config.get('koji_build_completed', 'critpath_filepath')

trigger_rules_template = open(rules_template_file, 'r').read()
