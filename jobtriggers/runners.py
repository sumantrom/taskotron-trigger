import requests
import json

from . import config
from .exceptions import RunnerException


class BaseRunner(object):

    """
    Format:
    {
        'item': <name of item - envr, update id, koji tag, etc.>,
        'item_type': <koji_build, koji_tag, bodhi_id>,
        'taskname': <task to be scheduled>,
        'arch': <arch needed for execution, x86_64, i386 etc.>
    }
    """

    def trigger_job(self, item, item_type, taskname, arch, **kwargs):
        raise NotImplementedError


class StreamRunner(BaseRunner):

    def trigger_job(self, item, item_type, taskname, arch, **kwargs):
        output = ["StreamRunner received:",
                  "item: %s" % item,
                  "item_type: %s" % item_type,
                  "taskname: %s" % taskname,
                  "arch: %s" % arch]

        output += ["%s: %s" % (k, w) for k, w in kwargs.iteritems()]
        yield '\n'.join(output)


class BuildbotRunner(BaseRunner):

    def trigger_job(self, item, item_type, taskname, arch, **kwargs):
        output = ["Buildbot received:",
                  "item: %s" % item,
                  "item_type: %s" % item_type,
                  "taskname: %s" % taskname,
                  "arch: %s" % arch]

        yield '\n'.join(output)

        properties = {
            'item': item,
            'item_type': item_type,
            'taskname': taskname,
            'arch': arch,
        }
        properties.update(kwargs)

        try:
            # Create new job in ExecDB
            req_uuid = requests.post("%s/jobs" % config.execdb_server,
                                     headers={'Content-type': 'application/json',
                                              'Accept': 'text/plain'},
                                     data=json.dumps(properties))

            if req_uuid.status_code != 201:
                raise RunnerException("ExecDB - bad status code: %s" % req_uuid.status_code)
            else:
                uuid = req_uuid.json()['uuid']

        except requests.exceptions.ConnectionError:
            yield "error: connection to ExecDB failed; %r" % properties

        except RunnerException as e:
            yield "error: %s; %r" % (e, properties)

        else:
            properties['uuid'] = uuid

            r = requests.post(config.buildbot_url,
                              data={'author': 'taskotron',
                                    'project': 'rpmcheck',
                                    'category': arch,
                                    'repository': '',
                                    'comments': 'build request from taskotron-trigger',
                                    'properties': json.dumps(properties)})

            yield "post-hook build request status: %s" % str(r.status_code)


_runners = {'StreamRunner': StreamRunner, 'BuildbotRunner': BuildbotRunner}


def get_runner(runner_type):
    return _runners[runner_type]()
