import fedmsg
import fedmsg.encoding

from .jobtrigger import JobTrigger

ITEM_TYPE = 'compose'


class ComposeCompletedJobTrigger(JobTrigger):
    topic = "org.fedoraproject.prod.compose.*"
    jsonify = False
    config_key = 'taskotron.composecompletedjobtrigger.enabled'

    def __init__(self, *args, **kw):
        super(ComposeCompletedJobTrigger, self).__init__(*args, **kw)

    def consume(self, message):
        msg = fedmsg.encoding.loads(message.body)

        topic = msg['topic']

        if not 'branched.complete' in topic and not 'rawhide.complete' in topic:
            return

        branch = msg['msg']['branch']
        arch = msg['msg']['arch']

        # non empty arch means secondary arch
        if arch:
            return

        self.log.debug('should be scheduling job for completed compose: %s' % branch)

        data = {
            "_msg": msg,
            "message_type": "ComposeCompleted",
            "item": branch,
            "item_type": ITEM_TYPE,
            "branch": branch,
        }
        self.do_trigger(data)
