import os
import copy
import yaml
import itertools
import fedmsg.consumers
from mongoquery import Query, QueryError

from . import config, runners, utils
from . import exceptions as exc
from .mongoquery_string_template import MongoTemplate


class DefaultDict(dict):

    """Since the rules will contain unreplacable variables (for example KojiBuildCompleted
    provides ${critpath_pkgs} and KojiTagChanged does not), we need a default value.
    None/null would be probably safer, but to make debugging easier, let's replace the missing
    keys with stringified placeholder instead.
    """

    def __missing__(self, key):
        return str("MISSING_VAR_%s" % key)


class JobTrigger(fedmsg.consumers.FedmsgConsumer):

    def __init__(self, *args, **kw):
        super(JobTrigger, self).__init__(*args, **kw)
        self.runner = runners.get_runner(config.runner_type)

    def trigger_tasks(self, item, item_type, tasks, arches, **kwargs):
        for task, arch in itertools.product(tasks, arches):
            self.log.info('triggering %s for %s on %s', task, item, arch)

            if config.job_logging:
                try:
                    utils.log_job(config.joblog_file, item, item_type, task, arch)
                except IOError, e:
                    self.log.exception(e)

            output = self.runner.trigger_job(item, item_type, task, arch, **kwargs)
            for line in output:
                self.log.info(line)

    def _tasks_from_rule(self, rule, message_data):
        """Takes a rule-dict as input, and produces list of dicts representing arguments required
        by the trigger_tasks method (i.e. item, item_type, tasks, arches) plus any additional data
        provided in the rule.

        """

        try:
            task_template = {
                'item': message_data['item'],
                'item_type': message_data['item_type'],
                'arches': config.valid_arches,
                'tasks': []
            }
        except KeyError, e:
            raise exc.TriggerMsgError("Missing data from message: %s" % e)

        tasks = []
        for item in rule['do']:
            # TODO: make this more efficient
            task = copy.deepcopy(task_template)

            if 'tasks' in item:
                task['tasks'] = item['tasks']
            elif 'discover' in item:
                discover = item['discover']

                # clone the repo (or fail)
                repo = discover['repo']
                branch = discover.get('branch', 'master').strip() or 'master'
                fallback = discover.get('fallback_branch', '').strip()
                repo_dir, branch = utils.clone_repo(repo, branch, fallback)

                # append base_dir, if specified
                base_dir = discover.get('base_dir', '').strip()
                repo_dir = os.path.join(repo_dir, base_dir)
                # fill the task details
                recursive = discover.get('recursive', False)
                task['repo'] = repo
                task['branch'] = branch
                task['tasks'] = utils.discover_tasks(repo_dir, task['item_type'], recursive)
            else:
                self.log.error("Rule has invalid `do` section: %r", rule)

            tasks.append(task)

        return tasks

    def _load_rules(self, template, data):
        """Taken out for ease of testing. This method takes the rules template and
        (message) data, replaces the variables in the template, and finally puts
        the string (now containing data provided from the respective FedMsg consumer),
        to the yaml parser.
        Keys that would be needed in template, but are not present in data are replaced
        by the default value provided by DefaultDict"""

        rules_yaml = MongoTemplate(template).substitute(DefaultDict(data))

        try:
            rules = yaml.load(rules_yaml)
        except yaml.YAMLError, e:
            raise exc.TriggerError(e)
        return rules

    def do_trigger(self, message_data):
        rules = self._load_rules(config.trigger_rules_template, message_data)

        tasks = []
        for rule in rules:
            if not rule.get('enabled', True):
                continue
            if 'message_type' in rule['when']:
                if rule['when']['message_type'] != message_data['message_type']:
                    continue
            try:
                if Query(rule['when']).match(message_data):
                    tasks.extend(self._tasks_from_rule(rule, message_data))
            except QueryError, e:
                self.log.debug(e)

        for task in tasks:
            try:
                self.trigger_tasks(**task)
            except exc.TriggerMsgError, e:
                self.log.debug(e)
            except exc.TriggerError, e:
                self.log.error(e)
