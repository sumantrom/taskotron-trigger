import koji
import fedmsg
import fedmsg.encoding

from . import config, utils
from .jobtrigger import JobTrigger
from . import exceptions as exc

ITEM_TYPE_PACKAGE = 'koji_build'
ITEM_TYPE_DOCKER = 'docker_image'


class KojiBuildCompletedJobTrigger(JobTrigger):
    topic = "org.fedoraproject.prod.buildsys.build.state.change"
    jsonify = False
    config_key = 'taskotron.kojibuildcompletedjobtrigger.enabled'

    def get_nvr(self, msg):
        release = msg['msg']['release']
        name = msg['msg']['name']
        version = msg['msg']['version']

        return name, version, release

    def process(self, msg):
        state = msg['msg']['new']
        instance = msg['msg']['instance']
        owner = msg['msg']['owner']
        name, version, release = self.get_nvr(msg)
        nvr = '-'.join((name, version, release))

        if not instance == 'primary':
            raise exc.TriggerMsgError('rejecting message not from primary koji instance '
                                      '(%s from %s)' % (nvr, instance))

        if state != koji.BUILD_STATES['COMPLETE']:
            raise exc.TriggerMsgError('rejecting message for not being a build complete message '
                                      '(%s, status %s)' % (nvr, state))

        # FIXME This is probably a temporary approach, could change in future, see D1002#19239
        if owner == 'containerbuild':
            return self._process_docker(msg)
        else:
            if 'fc' not in release:
                raise exc.TriggerMsgError('rejecting message for not containing "fc" in release '
                                          '(%s)' % nvr)
            return self._process_package(msg)

    def _process_package(self, msg):
        name, version, release = self.get_nvr(msg)
        item = '-'.join([name, version, release])
        critpath = utils.parse_yaml_from_file(config.critpath_filepath)
        distgit_branch = utils.get_distgit_branch(release)
        critpath_pkgs = critpath['pkgs'].get(distgit_branch, critpath['pkgs']['master'])
        data = {
            "_msg": msg,
            "message_type": "KojiBuildPackageCompleted",
            "item": item,
            "item_type": ITEM_TYPE_PACKAGE,
            "name": name,
            "version": version,
            "release": release,
            "critpath_pkgs": critpath_pkgs,
            "distgit_branch": distgit_branch,
        }
        self.do_trigger(data)

    def _process_docker(self, msg):
        name, version, release = self.get_nvr(msg)
        build_id = msg['msg']['build_id']
        item = utils.get_docker_registry_url(build_id)
        distgit_branch = utils.get_distgit_branch_docker(build_id)
        critpath_pkgs = []  # FIXME do we need this for docker images?
        data = {
            "_msg": msg,
            "message_type": "KojiBuildDockerCompleted",
            "item": item,
            "item_type": ITEM_TYPE_DOCKER,
            "name": name,
            "version": version,
            "release": release,
            "critpath_pkgs": critpath_pkgs,
            "distgit_branch": distgit_branch,
        }
        self.do_trigger(data)

    def consume(self, message):
        msg = fedmsg.encoding.loads(message.body)
        try:
            self.process(msg)
        except exc.TriggerMsgError, e:
            self.log.debug(e)
            return
