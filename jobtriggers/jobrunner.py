import csv
import argparse
import itertools
import logging
from copy import deepcopy

import requests
from . import config
from . import koji_build_msg
from . import koji_tag_msg
from . import exceptions as exc
from .runners import get_runner


runner = get_runner(config.runner_type)

# set up logging
logger = logging.getLogger('Jobrunner')
sh = logging.StreamHandler()
formatter = logging.Formatter(fmt='%(asctime)s - %(name)s - %(levelname)s: %(message)s',
                              datefmt='%Y-%m-%d %H:%M:%S')
sh.setFormatter(formatter)
logger.addHandler(sh)


def trigger_task(item, item_type, task, arch, **kwargs):
    logger.debug('triggering %s for %s on %s' % (task, item, arch))

    output = runner.trigger_job(item, item_type, task, arch, **kwargs)
    for line in output:
        logger.info(line)


def trigger_tasks(item, item_type, tasks, arches, **kwargs):
    for task, arch in itertools.product(tasks, arches):
        trigger_task(item, item_type, task, arch, **kwargs)


def file_jobs(jobsfile):
    with open(jobsfile, 'r') as csvfile:
        jobs = csv.reader(csvfile, delimiter=';')
        for job in jobs:
            trigger_task(job[1], job[2], job[3], job[4])


def datagrepper_jobs(start, end):
    params = {
        'start': start,
        'end': end,
        'category': 'buildsys',
        'topic': ['org.fedoraproject.prod.buildsys.build.state.change',
                  'org.fedoraproject.prod.buildsys.tag'],
        'page': 1,
    }

    while True:
        r = requests.get(config.datagrepper_url, params=params)
        pages = r.json()['pages']
        page = r.json()['arguments']['page']

        logger.info('Processing jobs from datagrepper: page %d of %d', page, pages)

        koji_tag_items = set()
        for msg in r.json()['raw_messages']:
            if msg['topic'].endswith('build.state.change'):
                try:
                    koji_build_msg.trigger_tasks(msg, trigger_tasks)
                except exc.TriggerMsgError, e:
                    logger.debug(e)
                except exc.TriggerError, e:
                    logger.error(e)
            elif msg['topic'].endswith('tag'):
                try:
                    koji_tag_items.add(koji_tag_msg.get_item(msg))
                except exc.TriggerMsgError, e:
                    logger.debug(e)
            else:
                logger.warning('Got an unexpected topic %s', msg['topic'])

        item_type = koji_tag_msg.ITEM_TYPE
        for item in koji_tag_items:
            tasks = deepcopy(config.koji_tag_changed_tasks)
            if item.endswith('testing-pending'):
                tasks.remove('upgradepath')
            trigger_tasks(item, item_type, tasks, config.valid_arches)

        next_page = page + 1
        if next_page > pages:
            break
        params['page'] = next_page


def main():
    parser = argparse.ArgumentParser(prog='Jobrunner',
                                     usage='Gets jobs from datagrepper from a given time '
                                           'period and runs them.')
    parser.add_argument('-s', '--start', help='Start time (Unix time format)')
    parser.add_argument('-e', '--end', help='End time (Unix time format)')
    parser.add_argument('-d', '--debug', action='store_true', help='Print debug message')
    parser.add_argument('-f', '--jobsfile', help='A file containing jobs in csv format')
    args = parser.parse_args()

    logger.setLevel(logging.DEBUG if args.debug else logging.INFO)

    # let's make running jobs from the file a priority
    if args.jobsfile:
        file_jobs(args.jobsfile)
    else:
        datagrepper_jobs(args.start, args.end)
