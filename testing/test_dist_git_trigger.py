import pytest
import mock
from dingus import Dingus
from munch import Munch

from jobtriggers import dist_git_commit_msg
from jobtriggers import utils

def mock_clone_repo(repo, branch='master', fallback=None):
    return '/tmp/path_to_repo', branch

@pytest.mark.usefixtures('prepare')
class TestDistGitCommitJobTrigger():

    @pytest.fixture
    def prepare(self, monkeypatch):
        self.item_type = 'dist_git_commit'
        self.ref_message = self._create_msg()
        self.ref_tasks = ['dist_git_task1', 'dist_git_task2']
        self.message_type = 'DistGitRPMCommit'

        stub_hub = Munch(config=Munch(get=0))

        self.helper = dist_git_commit_msg.DistGitCommitJobTrigger(stub_hub)
        self.helper.runner = Dingus()

        dist_git_commit_msg.config.trigger_rules_template = """---
- when: {message_type: DistGitCommit, namespace: rpms}
  do:
    - {discover: {repo: 'http://foo/test-rpms/${name}.git'}}
    - {tasks: [speclint]}  # Whatever speclint is..
- when: {message_type: DistGitCommit, namespace: modules}
  do:
    - {discover: {repo: 'http://foo/test-modules/${name}.git'}}
    - {tasks: [modlint]}   # modlint is a real thing :)
"""

        dist_git_commit_msg.config.job_logging = False

        mock_discover_tasks = mock.Mock(return_value=[])
        monkeypatch.setattr(utils, 'discover_tasks', mock_discover_tasks)
        monkeypatch.setattr(utils, 'clone_repo', mock_clone_repo)

        mock_critpath = {"pkgs": {"f18": [], "master": []}}
        mock_get_critpath = mock.Mock(return_value=mock_critpath)
        monkeypatch.setattr(utils, 'parse_yaml_from_file', mock_get_critpath)

    def _create_msg(self):
        return Munch(body={
            "i": 1,
            "msg": {
                "commit": {
                    "agent": "martinkg",
                    "branch": "f25",
                    "email": "mgansser@alice.de",
                    "message": "Update to 1.1.61\n",
                    "name": "Martin Gansser",
                    "namespace": "rpms",
                    "path": "/srv/git/repositories/rpms/vdr-epg-daemon.git",
                    "repo": "vdr-epg-daemon",
                    "rev": "6e5218a6e0b18c5a739e2164f85436f2ada5e475",
                    "seen": False,
                    "stats": {
                        "files": {
                            ".gitignore": {
                                "additions": 1,
                                "deletions": 0,
                                "lines": 1
                            },
                            "clog": {
                                "additions": 1,
                                "deletions": 1,
                                "lines": 2
                            },
                            "sources": {
                                "additions": 1,
                                "deletions": 1,
                                "lines": 2
                            },
                            "vdr-epg-daemon.spec": {
                                "additions": 4,
                                "deletions": 1,
                                "lines": 5
                            }
                        },
                        "total": {
                            "additions": 7,
                            "deletions": 3,
                            "files": 4,
                            "lines": 10
                        }
                    },
                    "summary": "Update to 1.1.61",
                    "username": "martinkg"
                }
            },
            "msg_id": "2016-c0f8f391-7936-4c4e-9401-13c0c2372102",
            "timestamp": 1480707154.0,
            "topic": "org.fedoraproject.prod.git.receive"
        })

    def test_consume(self):
        self.helper.consume(self.ref_message)

        runner_calls = self.helper.runner.trigger_job.calls()

        assert len(runner_calls) == 1

        assert runner_calls[0][1] == (
            'rpms/vdr-epg-daemon#6e5218a6e0b18c5a739e2164f85436f2ada5e475',
            'dist_git_commit',
            'speclint',
            'x86_64',
        )
