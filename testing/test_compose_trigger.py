import pytest
from dingus import Dingus
from munch import Munch

from jobtriggers import jobtrigger
from jobtriggers import compose_complete_msg


@pytest.mark.usefixtures('prepare')
class TestComposeCompletedJobTrigger():

    @pytest.fixture
    def prepare(self, monkeypatch):
        self.ref_arch = ''
        self.ref_branch = '21'
        self.ref_topic = 'org.fedoraproject.prod.compose.branched.complete'

        self._create_msg(self.ref_arch, self.ref_branch, self.ref_topic)

        self.ref_tasks = ['compose_task1', 'compose_task2']
        self.ref_arches = 'x86_64'

        stub_hub = Munch(config=Munch(get=0))

        self.helper = compose_complete_msg.ComposeCompletedJobTrigger(stub_hub)
        self.helper.runner = Dingus()

        jobtrigger.config.compose_completed_tasks = self.ref_tasks
        jobtrigger.config.valid_arches = [self.ref_arches]
        jobtrigger.config.job_logging = False

        jobtrigger.config.trigger_rules_template = """---
- do:
  - {tasks: %r}
  when: {message_type: ComposeCompleted}
""" % (self.ref_tasks)

    def _create_msg(self, ref_arch, ref_branch, ref_topic):
        self.ref_message = Munch(body='{"i": 1,\
                                  "msg": {"arch": "%s",\
                                          "branch": "%s",\
                                          "log": "done"},\
                                  "topic": "%s"}' %
                                 (ref_arch, ref_branch, ref_topic))

    def test_consume(self):
        self.helper.consume(self.ref_message)

        runner_calls = self.helper.runner.trigger_job.calls()

        assert len(runner_calls) == 2

        assert runner_calls[0][1] == (
            self.ref_branch, compose_complete_msg.ITEM_TYPE, self.ref_tasks[0], self.ref_arches)
        assert runner_calls[1][1] == (
            self.ref_branch, compose_complete_msg.ITEM_TYPE, self.ref_tasks[1], self.ref_arches)

    def test_consume_secondary_arch(self):
        ref_arch = 's390'
        self._create_msg(ref_arch, self.ref_branch, self.ref_topic)

        self.helper.consume(self.ref_message)

        runner_calls = self.helper.runner.trigger_job.calls()

        assert len(runner_calls) == 0

    def test_consume_incorrect_topic(self):
        ref_topic = 'fake_topic'
        self._create_msg(self.ref_arch, self.ref_branch, ref_topic)

        self.helper.consume(self.ref_message)

        runner_calls = self.helper.runner.trigger_job.calls()

        assert len(runner_calls) == 0

    def test_consume_allowed_topics(self):
        ref_topics = ['org.fedoraproject.prod.compose.branched.complete',
                      'org.fedoraproject.prod.compose.rawhide.complete']

        for ref_topic in ref_topics:
            self._create_msg(self.ref_arch, self.ref_branch, ref_topic)

            self.helper.consume(self.ref_message)

            runner_calls = self.helper.runner.trigger_job.calls()

        assert len(runner_calls) == 4
