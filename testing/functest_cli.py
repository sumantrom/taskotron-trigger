from dingus import Dingus

import jobtriggers.jobrunner
from jobtriggers.utils import log_job


class TestCli():

    def setup_method(self, method):
        self.ref_tests = ['rpmlint', 'upgradepath']
        self.ref_unit_types = ['koji_build', 'bodhi_id']
        self.ref_nvr = 'xchat-2.8.8-21.fc20'
        self.ref_arch = 'x86_64'

    def test_run_jobs(self, tmpdir, monkeypatch):
        f = tmpdir.join('jobs.csv')
        filename = str(f.realpath())

        monkeypatch.setattr(jobtriggers.jobrunner, 'runner', Dingus())

        log_job(filename, self.ref_tests[0], self.ref_unit_types[
                0], self.ref_nvr, self.ref_arch)
        log_job(filename, self.ref_tests[1], self.ref_unit_types[
                1], self.ref_nvr, self.ref_arch)

        jobtriggers.jobrunner.file_jobs(filename)

        runner_calls = jobtriggers.jobrunner.runner.trigger_job.calls()

        assert len(runner_calls) == 2

        assert runner_calls[0][1] == (
            self.ref_tests[0], self.ref_unit_types[0], self.ref_nvr, self.ref_arch)
        assert runner_calls[1][1] == (
            self.ref_tests[1], self.ref_unit_types[1], self.ref_nvr, self.ref_arch)
