import pytest
from dingus import Dingus
from munch import Munch
from copy import deepcopy
import fedmsg.encoding

from jobtriggers import koji_tag_msg


@pytest.mark.usefixtures('prepare')
class TestKojiTagChangedJobTrigger():

    @pytest.fixture
    def prepare(self, monkeypatch):
        self.ref_instance = 'primary'
        self.ref_name = 'stage'
        self.ref_version = '4.1.1'
        self.ref_release = '3.fc18'
        self.ref_tag = 'f18-updates-pending'
        self.ref_nvr = '%s-%s-%s' % (self.ref_name,
                                     self.ref_version, self.ref_release)

        self.ref_tasks = ['depcheck', 'upgradepath']
        self.ref_validarches = ['i386', 'x86_64']

        self._create_msg(
            self.ref_instance, self.ref_name, self.ref_release, self.ref_tag, self.ref_version)

        self.ref_data = {
            "_msg": {},
            "message_type": "KojiTagChanged",
            "item": self.ref_tag,
            "item_type": "koji_tag",
                         "tag": self.ref_tag,
        }

        stub_hub = Munch(config=Munch(get=0))
        self.helper = koji_tag_msg.KojiTagChangedJobTrigger(stub_hub)

        self.helper.runner = Dingus()
        koji_tag_msg.config.trigger_rules_template = """---
- do:
  - {tasks: [depcheck]}
  when: {message_type: KojiTagChanged}
- do:
  - {tasks: [upgradepath]}
  when: {message_type: KojiTagChanged, tag: {$regex: '/.*(?<!testing-pending)$$/'}}
"""

        koji_tag_msg.config.valid_arches = self.ref_validarches
        koji_tag_msg.config.job_logging = False

    def _create_msg(self, ref_instance, ref_name, ref_release, ref_tag, ref_version):
        self.ref_message = Munch(body='{"i": 1,\
                                        "msg": {"build_id": 221146,\
                                                "instance": "%s",\
                                                "name": "%s",\
                                                "owner": "ralph",\
                                                "release": "%s",\
                                                "tag": "%s",\
                                                "tag_id": 216,\
                                                "user": "bodhi",\
                                                "version": "%s"},\
                                        "timestamp": 1359603469.21164,\
                                        "topic": "org.fedoraproject.prod.buildsys.tag",\
                                        "username": "apache"}' %
                                 (ref_instance, ref_name, ref_release, ref_tag, ref_version))

    def test_delayed_queue(self):
        self.helper.consume(self.ref_message)

        assert self.helper.queued_tags == set([self.ref_tag])
        assert self.helper.queued_data[self.ref_tag] == {
            "_msg": fedmsg.encoding.loads(self.ref_message.body),
            "message_type": "KojiTagChanged",
            "item": self.ref_tag,
            "item_type": "koji_tag",
            "tag": self.ref_tag,
        }

    def test_delayed_consume(self):
        self.helper.queued_tags = [self.ref_tag]
        self.helper.queued_data = {self.ref_tag: self.ref_data}

        self.helper.delayed_consume()

        runner_calls = self.helper.runner.trigger_job.calls()

        assert len(runner_calls) == 4

        assert runner_calls[0][1] == (
            self.ref_tag, koji_tag_msg.ITEM_TYPE, self.ref_tasks[0], self.ref_validarches[0])
        assert runner_calls[1][1] == (
            self.ref_tag, koji_tag_msg.ITEM_TYPE, self.ref_tasks[0], self.ref_validarches[1])
        assert runner_calls[2][1] == (
            self.ref_tag, koji_tag_msg.ITEM_TYPE, self.ref_tasks[1], self.ref_validarches[0])
        assert runner_calls[3][1] == (
            self.ref_tag, koji_tag_msg.ITEM_TYPE, self.ref_tasks[1], self.ref_validarches[1])

        assert self.helper.queued_tags == set()
        assert self.helper.queued_data == dict()

    def test_consume_not_primary_instance(self):
        ref_instance = 'ppc'
        self._create_msg(
            ref_instance, self.ref_name, self.ref_release, self.ref_tag, self.ref_version)

        self.helper.consume(self.ref_message)

        runner_calls = self.helper.runner.trigger_job.calls()

        assert runner_calls == []

    def test_consume_kojitag_not_pending(self):
        ref_tag = 'f18-updates-testing'
        self._create_msg(
            self.ref_instance, self.ref_name, self.ref_release, ref_tag, self.ref_version)
        self.helper.consume(self.ref_message)

        assert self.helper.queued_tags == set()

    def test_delayed_consume_upgradepath(self):
        ref_tag = 'f18-updates-testing-pending'
        ref_data = deepcopy(self.ref_data)
        ref_data['item'] = ref_data['tag'] = ref_tag

        self.helper.queued_tags = [ref_tag]
        self.helper.queued_data = {ref_tag: ref_data}

        self.helper.delayed_consume()

        runner_calls = self.helper.runner.trigger_job.calls()

        assert len(runner_calls) == 2

        assert runner_calls[0][1] == (
            ref_tag, koji_tag_msg.ITEM_TYPE, self.ref_tasks[0], self.ref_validarches[0])
        assert runner_calls[1][1] == (
            ref_tag, koji_tag_msg.ITEM_TYPE, self.ref_tasks[0], self.ref_validarches[1])

        assert self.helper.queued_tags == set()

    def test_consume_blacklisted_release(self):
        ref_release = '3.el7.1'
        ref_tag = 'epel7-testing-pending'
        self._create_msg(
            self.ref_instance, self.ref_name, ref_release, ref_tag, self.ref_version)

        self.helper.consume(self.ref_message)

        assert self.helper.queued_tags == set([])

    def test_consume_allowed_release(self):
        ref_release = '3.fc20.1'
        ref_tag = 'f20-updates-testing-pending'
        self._create_msg(
            self.ref_instance, self.ref_name, ref_release, ref_tag, self.ref_version)

        self.helper.consume(self.ref_message)

        assert self.helper.queued_tags == set([ref_tag])

    def test_ignore_signing_tags(self):
        ''' Now that there are *-signing-pending tags in koji, we need to be more selective about
        what we're triggering on
        '''

        ref_release = '3.fc20.1'
        ref_tag = 'f20-signing-pending'
        self._create_msg(
            self.ref_instance, self.ref_name, ref_release, ref_tag, self.ref_version)

        self.helper.consume(self.ref_message)

        assert self.helper.queued_tags == set([])
