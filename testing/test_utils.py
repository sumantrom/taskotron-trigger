import os
import subprocess

import pytest
import mock

from jobtriggers import utils
from jobtriggers import config
from jobtriggers import exceptions as exc


@pytest.mark.usefixtures('prepare')
class TestUtils():

    @pytest.fixture
    def prepare(self, monkeypatch):
        pass

    def test_check_yaml_scanning_error(self):
        file_data = '%'
        mocked_open = mock.mock_open(read_data=file_data)

        with mock.patch('__builtin__.open', mocked_open):
            with pytest.raises(exc.TriggerError):
                utils.parse_yaml_from_file('mocked_filename')

    def test_check_yaml_parsing_error(self):
        file_data = ':'
        mocked_open = mock.mock_open(read_data=file_data)

        with mock.patch('__builtin__.open', mocked_open):
            with pytest.raises(exc.TriggerError):
                utils.parse_yaml_from_file('mocked_filename')

    def test_check__should_schedule_true(self):
        file_data = '''
            scheduling:
                run: True

            input:
                args:
                    - koji_build
            '''
        mocked_open = mock.mock_open(read_data=file_data)

        with mock.patch('__builtin__.open', mocked_open):
            assert utils._should_schedule('mocked_filename', 'koji_build')

    def test_check__should_schedule_false(self):
        file_data = '''
            scheduling:
                run: False

            input:
                args:
                    - koji_build
            '''
        mocked_open = mock.mock_open(read_data=file_data)

        with mock.patch('__builtin__.open', mocked_open):
            assert not utils._should_schedule('mocked_filename', 'koji_build')

    def test_check__should_schedule_default(self):
        file_data = '''
            input:
                args:
                    - koji_build
            '''
        mocked_open = mock.mock_open(read_data=file_data)

        with mock.patch('__builtin__.open', mocked_open):
            assert utils._should_schedule('mocked_filename', 'koji_build')

    def test_check__should_schedule_false_item_type(self):
        file_data = '''
            input:
                args:
                    - koji_tag
            '''
        mocked_open = mock.mock_open(read_data=file_data)

        with mock.patch('__builtin__.open', mocked_open):
            assert not utils._should_schedule('mocked_filename', 'koji_build')

    def test_clone_repo(self, monkeypatch):
        mock_path_exists = mock.Mock(return_value=False)
        mock_check_output = mock.Mock(return_value='')

        monkeypatch.setattr(os.path, 'exists', mock_path_exists)
        monkeypatch.setattr(config, 'git_cache_dir', '/path/to')
        monkeypatch.setattr(subprocess, 'check_output', mock_check_output)

        repo_dir, branch = utils.clone_repo('http://bogus.url/repo')
        assert repo_dir == '/path/to/repo'
        assert branch == 'master'

        repo_dir, branch = utils.clone_repo('http://bogus.url/repo.git', branch='develop')
        assert repo_dir == '/path/to/repo'
        assert branch == 'develop'

        repo_dir, branch = utils.clone_repo('http://bogus.url/repo', fallback='fallback')
        assert repo_dir == '/path/to/repo'
        assert branch == 'master'

    def test_clone_repo_missing_brach(self, monkeypatch):
        mock_path_exists = mock.Mock(return_value=False)
        def mock_check_output(params, *args, **kwargs):
            if 'ok_branch' not in params:
                exc = subprocess.CalledProcessError(None, None, 'Could not find remote branch')
                raise exc
            return

        monkeypatch.setattr(os.path, 'exists', mock_path_exists)
        monkeypatch.setattr(config, 'git_cache_dir', '/path/to')
        monkeypatch.setattr(subprocess, 'check_output', mock_check_output)

        # sanity check to be sure the mock_check_output works
        repo_dir, branch = utils.clone_repo('/repo', branch='ok_branch')
        assert repo_dir == '/path/to/repo'
        assert branch == 'ok_branch'
        with pytest.raises(exc.TriggerError):
            utils.clone_repo('', branch='missing_branch')

        # actual check
        repo_dir, branch = utils.clone_repo('/repo', branch='master', fallback='ok_branch')
        assert repo_dir == '/path/to/repo'
        assert branch == 'ok_branch'

        with pytest.raises(exc.TriggerError):
            utils.clone_repo('', branch='missing_branch', fallback='master')

    def test__discover_formulae(self, monkeypatch):
        def mock_os_walk(directory):
            yield (directory, ['task_1', 'nontask'], ['runtask.yml'])
            yield ('%s/nontask' % directory, [], [])
            yield ('%s/task_1' % directory, [], ['runtask.yml'])

        monkeypatch.setattr(os, 'walk', mock_os_walk)

        formulae = utils._discover_formulae('  /path/to/   ', recursive=True)
        assert len(formulae) == 2
        assert formulae == ['runtask.yml', 'task_1/runtask.yml']

        formulae = utils._discover_formulae('/path/to')
        assert len(formulae) == 1
        assert formulae == ['runtask.yml']

    def test_discover_tasks(self, monkeypatch):
        mock__discover_formulae = mock.Mock(return_value=['task1.yml', 'task2.yml'])
        def mock__should_schedule(task, item_type):
            return task == '/path/to/git/task2.yml' and item_type == 'fake_type'

        monkeypatch.setattr(utils, '_discover_formulae', mock__discover_formulae)
        monkeypatch.setattr(utils, '_should_schedule', mock__should_schedule)

        tasks = utils.discover_tasks('/path/to/git', 'fake_type')
        assert len(tasks) == 1
        assert tasks == ['task2.yml']

class TestGetBranch(object):

    def test_nvrs(self):
        assert utils.get_distgit_branch('foo-1.2-3.fc20') == 'f20'
        assert utils.get_distgit_branch('foo-1.2-3.fc20.1') == 'f20'
        assert utils.get_distgit_branch('foo-1.2-3.fc22hashgit.fc20') == 'f20'
        assert utils.get_distgit_branch('foo-1.2-3.fc22hashgit.fc20.1') == 'f20'

    def test_unsupported_dist_tag(self):
        with pytest.raises(exc.TriggerMsgError):
            utils.get_distgit_branch('foo-1.2-3.el7')
