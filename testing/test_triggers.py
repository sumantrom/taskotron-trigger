import pytest

from jobtriggers import runners


class TestRunners():

    def setup_method(self, method):
        self.ref_runnername = 'StreamRunner'

    def test_get_valid_runner(self):
        res = runners.get_runner(self.ref_runnername)

        assert isinstance(res, runners.StreamRunner)

    def test_get_invalid_runner(self):
        with pytest.raises(KeyError):
            runners.get_runner('NotExistingRandomRunner')
