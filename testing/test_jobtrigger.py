import pytest
import mock
import copy
from dingus import Dingus
from munch import Munch

from jobtriggers import jobtrigger
from jobtriggers import utils
from jobtriggers import exceptions as exc


class BogusJobTrigger(jobtrigger.JobTrigger):
    config_key = 'taskotron.bogusjobtrigger.enabled'

def mock_clone_repo(repo, branch='master', fallback=None):
    return '/tmp/path_to_repo', branch


@pytest.mark.usefixtures('prepare')
class TestJobtrigger():

    @pytest.fixture
    def prepare(self, monkeypatch):
        stub_hub = Munch(config=Munch(get=0))
        self.helper = BogusJobTrigger(stub_hub)
        self.helper.runner = Dingus()

        self.ref_discovered_tasks = ['discovered_task_1']
        mock_discover_tasks = mock.Mock(return_value=self.ref_discovered_tasks)
        monkeypatch.setattr(utils, 'discover_tasks', mock_discover_tasks)

        monkeypatch.setattr(utils, 'clone_repo', mock_clone_repo)

        self.ref_item = "package_foobar-1.0.8-fc18"
        self.ref_item_type = "bar"
        self.ref_name = "package_foobar"
        self.ref_message_data = {
            "item": self.ref_item, "item_type": self.ref_item_type, "name": self.ref_name}
        self.ref_default_arches = ['i386', 'x86_64']

        self.ref_tasks = ['ref_task1', 'ref_task2']
        self.ref_rules = [
            {'do': [{'discover': {'repo': 'http://bogus.repo/${name}.git'}}],
             'when': {'message_type': 'DiscoverTasks'}},
            {'do': [{'tasks': self.ref_tasks}],
             'when': {'message_type': 'RunTasks'}},
            {'do': [{'tasks': self.ref_tasks}],
             'when': {'message_type': 'DisabledTasks'},
             'enabled': False},
        ]

        jobtrigger.config.valid_arches = self.ref_default_arches

    def test_load_rules(self):
        template = """$in: [${foo}, ${in}]"""
        data = {"foo": "bar", "in": "out"}

        assert self.helper._load_rules(
            template, {}) == {"$in": ["MISSING_VAR_foo", "MISSING_VAR_in"]}
        assert self.helper._load_rules(
            template, data) == {"$in": ['bar', 'out']}

    def test__tasks_from_rule_exceptions(self):
        with pytest.raises(exc.TriggerMsgError):
            self.helper._tasks_from_rule(None, {})
        with pytest.raises(exc.TriggerMsgError):
            self.helper._tasks_from_rule(None, {'item': 'foo'})
        with pytest.raises(exc.TriggerMsgError):
            self.helper._tasks_from_rule(None, {'item_type': 'bar'})

    def test__tasks_from_rule(self):
        rule = {'do': [{'tasks': self.ref_tasks}]}
        tasks = self.helper._tasks_from_rule(rule, self.ref_message_data)
        assert len(tasks) == 1
        assert tasks[0]['tasks'] == self.ref_tasks
        assert tasks[0]['item'] == self.ref_message_data['item']
        assert tasks[0]['item_type'] == self.ref_message_data['item_type']
        assert tasks[0]['arches'] == self.ref_default_arches

    def test__tasks_from_rule_discover(self):
        ref_repo = 'http://bogus.repo/firefox.git'
        rule = {'do': [{'discover': {'repo': ref_repo}}]}
        tasks = self.helper._tasks_from_rule(rule, self.ref_message_data)
        assert len(tasks) == 1
        assert tasks[0]['tasks'] == self.ref_discovered_tasks
        assert tasks[0]['item'] == self.ref_message_data['item']
        assert tasks[0]['item_type'] == self.ref_message_data['item_type']
        assert tasks[0]['repo'] == ref_repo
        assert tasks[0]['branch'] == 'master'

    def test__tasks_from_rule_discover_set_branch(self):
        ref_repo = 'http://bogus.repo/firefox.git'
        rule = {'do': [{'discover': {'repo': ref_repo, 'branch': 'mybranch'}}]}
        tasks = self.helper._tasks_from_rule(rule, self.ref_message_data)
        assert len(tasks) == 1
        assert tasks[0]['tasks'] == self.ref_discovered_tasks
        assert tasks[0]['item'] == self.ref_message_data['item']
        assert tasks[0]['item_type'] == self.ref_message_data['item_type']
        assert tasks[0]['repo'] == ref_repo
        assert tasks[0]['branch'] == 'mybranch'

    def test__tasks_from_rule_discover_recursive(self, monkeypatch):
        utils_dingus = Dingus(discover_tasks__returns=self.ref_discovered_tasks)
        monkeypatch.setattr(utils, 'discover_tasks', utils_dingus.discover_tasks)

        ref_repo = 'http://bogus.repo/firefox.git'
        rule = {'do': [{'discover': {'repo': ref_repo, 'recursive': True}}]}
        tasks = self.helper._tasks_from_rule(rule, self.ref_message_data)

        discover_calls = utils_dingus.calls()
        assert len(discover_calls) == 1
        assert discover_calls[0][0] == 'discover_tasks'
        assert discover_calls[0][1][0] == '/tmp/path_to_repo/'
        assert discover_calls[0][1][1] == self.ref_message_data['item_type']
        assert discover_calls[0][1][2] == True

    def test_do_trigger_runtasks(self, monkeypatch):
        mock__load_rules = mock.Mock(return_value=self.ref_rules)
        monkeypatch.setattr(self.helper, '_load_rules', mock__load_rules)

        message_data = copy.deepcopy(self.ref_message_data)
        message_data['message_type'] = 'RunTasks'

        self.helper.do_trigger(message_data)

        runner_calls = self.helper.runner.trigger_job.calls()

        assert len(runner_calls) == 4

        assert runner_calls[0][1] == (
            self.ref_item, self.ref_item_type, self.ref_tasks[0], self.ref_default_arches[0])
        assert runner_calls[1][1] == (
            self.ref_item, self.ref_item_type, self.ref_tasks[0], self.ref_default_arches[1])
        assert runner_calls[2][1] == (
            self.ref_item, self.ref_item_type, self.ref_tasks[1], self.ref_default_arches[0])
        assert runner_calls[3][1] == (
            self.ref_item, self.ref_item_type, self.ref_tasks[1], self.ref_default_arches[1])

    def test_do_trigger_discover(self, monkeypatch):
        mock__load_rules = mock.Mock(return_value=self.ref_rules)
        monkeypatch.setattr(self.helper, '_load_rules', mock__load_rules)

        message_data = copy.deepcopy(self.ref_message_data)
        message_data['message_type'] = 'DiscoverTasks'

        self.helper.do_trigger(message_data)

        runner_calls = self.helper.runner.trigger_job.calls()

        assert len(runner_calls) == 2

        assert runner_calls[0][1] == (
            self.ref_item, self.ref_item_type, self.ref_discovered_tasks[0],
            self.ref_default_arches[0])
        assert runner_calls[1][1] == (
            self.ref_item, self.ref_item_type, self.ref_discovered_tasks[0],
            self.ref_default_arches[1])

    def test_do_trigger_disabled(self, monkeypatch):
        mock__load_rules = mock.Mock(return_value=self.ref_rules)
        monkeypatch.setattr(self.helper, '_load_rules', mock__load_rules)

        message_data = copy.deepcopy(self.ref_message_data)
        message_data['message_type'] = 'DisabledTasks'

        self.helper.do_trigger(message_data)

        runner_calls = self.helper.runner.trigger_job.calls()

        assert len(runner_calls) == 0
