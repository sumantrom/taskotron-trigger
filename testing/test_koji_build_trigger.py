import koji
import pytest
import mock
from dingus import Dingus
from munch import Munch

from jobtriggers import koji_build_msg
from jobtriggers import utils

params = [('someowner', koji_build_msg.ITEM_TYPE_PACKAGE, "KojiBuildPackageCompleted"),
          ('containerbuild', koji_build_msg.ITEM_TYPE_DOCKER, "KojiBuildDockerCompleted")]

def mock_clone_repo(repo, branch='master', fallback=None):
    return '/tmp/path_to_repo', branch

@pytest.mark.usefixtures('prepare')
@pytest.mark.parametrize('buildtype', params)
class TestKojiBuildCompletedJobTrigger():

    @pytest.fixture
    def prepare(self, monkeypatch, buildtype):
        self.ref_instance = 'primary'
        self.ref_new = koji.BUILD_STATES['COMPLETE']
        self.ref_name = 'stage'
        self.ref_version = '4.1.1'
        self.ref_release = '3.fc18'
        self.ref_owner = buildtype[0]
        self.item_type = buildtype[1]
        self.message_type = buildtype[2]
        self.ref_nvr = '%s-%s-%s' % (self.ref_name,
                                     self.ref_version, self.ref_release)
        self.ref_nvr_docker = '%s-%s-%s' % ('docker',
                                            self.ref_version, self.ref_release)

        self.ref_message = self._create_msg(self.ref_instance, self.ref_new, self.ref_name,
                                            self.ref_release, self.ref_version, self.ref_owner)

        self.ref_tasks = ['koji_build_task1', 'koji_build_task2']
        self.ref_tasks_docker = ['docker_build_task']
        self.ref_validarches = ['i386', 'x86_64']

        stub_hub = Munch(config=Munch(get=0))

        self.helper = koji_build_msg.KojiBuildCompletedJobTrigger(stub_hub)
        self.helper.runner = Dingus()

        koji_build_msg.config.trigger_rules_template = """---
- do:
  - {discover: {repo: 'http://bogus/firefox.git'}}
  - {tasks: %s}
  when: {message_type: %s}
- do:
  - {tasks: %s}
  when: {message_type: %s, name: docker}
""" % (self.ref_tasks, self.message_type, self.ref_tasks_docker, self.message_type)

        koji_build_msg.config.valid_arches = self.ref_validarches
        koji_build_msg.config.job_logging = False

        mock_discover_tasks = mock.Mock(return_value=[])
        monkeypatch.setattr(utils, 'discover_tasks', mock_discover_tasks)

        monkeypatch.setattr(utils, 'clone_repo', mock_clone_repo)

        mock_critpath = {"pkgs": {"f18": [], "master": []}}
        mock_get_critpath = mock.Mock(return_value=mock_critpath)
        monkeypatch.setattr(utils, 'parse_yaml_from_file', mock_get_critpath)

        stub_session = mock.MagicMock()
        stub_session.listArchives.return_value = [
            {'extra': {'docker': {'repositories': ['image@shaXXX', self.ref_nvr]}}}]
        stub_session.listTags.return_value = [{'name': 'ubuntu-docker'}, {'name': 'f24-docker'}]

        stub_get_session = mock.MagicMock(return_value=stub_session)
        monkeypatch.setattr(utils, '_get_koji_session', stub_get_session)

    def _create_msg(self, ref_instance, ref_new, ref_name, ref_release, ref_version, ref_owner):
        msg = Munch(body='{"i": 1,\
                           "msg": {"build_id": 221146,\
                                   "owner": "%s",\
                                   "instance": "%s",\
                                   "new": %d,\
                                   "name": "%s",\
                                   "release": "%s",\
                                   "version": "%s"},\
                           "timestamp": 1359603469.21164,\
                           "topic": "org.fedoraproject.prod.buildsys.build.state.change",\
                           "username": "apache"}' %
                    (ref_owner, ref_instance, ref_new, ref_name, ref_release, ref_version))
        return msg

    def test_consume(self):
        self.helper.consume(self.ref_message)

        runner_calls = self.helper.runner.trigger_job.calls()

        assert len(runner_calls) == 4

        assert runner_calls[0][1] == (self.ref_nvr, self.item_type,
                                      self.ref_tasks[0], self.ref_validarches[0])
        assert runner_calls[1][1] == (self.ref_nvr, self.item_type,
                                      self.ref_tasks[0], self.ref_validarches[1])
        assert runner_calls[2][1] == (self.ref_nvr, self.item_type,
                                      self.ref_tasks[1], self.ref_validarches[0])
        assert runner_calls[3][1] == (self.ref_nvr, self.item_type,
                                      self.ref_tasks[1], self.ref_validarches[1])

    def test_consume_docker(self):
        # skip test when item is docker image
        if self.item_type == koji_build_msg.ITEM_TYPE_DOCKER:
            return
        ref_name = 'docker'
        self.ref_message = self._create_msg(self.ref_instance, self.ref_new, ref_name,
                                            self.ref_release, self.ref_version, self.ref_owner)

        self.helper.consume(self.ref_message)

        runner_calls = self.helper.runner.trigger_job.calls()

        assert len(runner_calls) == 6

        assert runner_calls[0][1] == (self.ref_nvr_docker, self.item_type,
                                      self.ref_tasks[0], self.ref_validarches[0])
        assert runner_calls[1][1] == (self.ref_nvr_docker, self.item_type,
                                      self.ref_tasks[0], self.ref_validarches[1])
        assert runner_calls[2][1] == (self.ref_nvr_docker, self.item_type,
                                      self.ref_tasks[1], self.ref_validarches[0])
        assert runner_calls[3][1] == (self.ref_nvr_docker, self.item_type,
                                      self.ref_tasks[1], self.ref_validarches[1])
        assert runner_calls[4][1] == (self.ref_nvr_docker, self.item_type,
                                      self.ref_tasks_docker[0], self.ref_validarches[0])
        assert runner_calls[5][1] == (self.ref_nvr_docker, self.item_type,
                                      self.ref_tasks_docker[0], self.ref_validarches[1])

    def test_consume_distgit(self, monkeypatch):
        ref_distgit_task = 'abicheck'
        mock_discover_tasks = mock.Mock(return_value=[ref_distgit_task])
        monkeypatch.setattr(utils, 'discover_tasks', mock_discover_tasks)

        self.helper.consume(self.ref_message)

        runner_calls = self.helper.runner.trigger_job.calls()

        assert len(runner_calls) == 6

        assert runner_calls[0][1] == (self.ref_nvr, self.item_type,
                                      ref_distgit_task, self.ref_validarches[0])
        assert runner_calls[1][1] == (self.ref_nvr, self.item_type,
                                      ref_distgit_task, self.ref_validarches[1])
        assert runner_calls[2][1] == (self.ref_nvr, self.item_type,
                                      self.ref_tasks[0], self.ref_validarches[0])
        assert runner_calls[3][1] == (self.ref_nvr, self.item_type,
                                      self.ref_tasks[0], self.ref_validarches[1])
        assert runner_calls[4][1] == (self.ref_nvr, self.item_type,
                                      self.ref_tasks[1], self.ref_validarches[0])
        assert runner_calls[5][1] == (self.ref_nvr, self.item_type,
                                      self.ref_tasks[1], self.ref_validarches[1])

    def test_consume_not_primary_instance(self):
        ref_instance = 'ppc'
        self.ref_message = self._create_msg(ref_instance, self.ref_new, self.ref_name,
                                            self.ref_release, self.ref_version, self.ref_owner)

        self.helper.consume(self.ref_message)

        runner_calls = self.helper.runner.trigger_job.calls()

        assert runner_calls == []

    def test_consume_notcomplete_state(self):
        ref_new = koji.BUILD_STATES['BUILDING']
        self.ref_message = self._create_msg(self.ref_instance, ref_new, self.ref_name,
                                            self.ref_release, self.ref_version, self.ref_owner)

        self.helper.consume(self.ref_message)

        runner_calls = self.helper.runner.trigger_job.calls()

        assert runner_calls == []

    def test_consume_blacklisted_release(self):
        # skip test when item is docker image
        if self.item_type == koji_build_msg.ITEM_TYPE_DOCKER:
            return

        ref_release = '3.el5.1'
        self.ref_message = self._create_msg(self.ref_instance, self.ref_new, self.ref_name,
                                            ref_release, self.ref_version, self.ref_owner)

        self.helper.consume(self.ref_message)

        runner_calls = self.helper.runner.trigger_job.calls()

        assert runner_calls == []
