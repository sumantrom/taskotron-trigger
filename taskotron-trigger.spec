Name:           taskotron-trigger
Version:        0.4.3
Release:        1%{?dist}
Summary:        Triggering Taskotron jobs via fedmsg

License:        GPLv2+
URL:            https://bitbucket.org/fedoraqa/taskotron-trigger
Source0:        https://qa.fedoraproject.org/releases/%{name}/%{name}-%{version}.tar.gz

BuildArch:      noarch

Requires:       fedmsg
Requires:       fedmsg-hub
Requires:       git
Requires:       koji
Requires:       python-mongoquery
Requires:       python-requests
Requires:       python-twisted
Requires:       PyYAML
BuildRequires:  fedmsg
BuildRequires:  koji
BuildRequires:  pytest
BuildRequires:  python2-devel
BuildRequires:  python-dingus
BuildRequires:  python-mock
BuildRequires:  python-mongoquery
BuildRequires:  python-munch
BuildRequires:  python-setuptools
BuildRequires:  PyYAML

%description
Triggering Taskotron jobs via fedmsg.

%prep
%setup -q

%check
py.test testing
rm -f %{buildroot}%{_sysconfdir}/fedmsg.d/*.py{c,o}

%build
%{__python} setup.py build

%install
%{__python} setup.py install -O1 --skip-build --root %{buildroot}

install -d %{buildroot}%{_sysconfdir}/taskotron/
install -p -m 644 conf/trigger.cfg.example %{buildroot}%{_sysconfdir}/taskotron/trigger.cfg
install -p -m 644 conf/trigger_rules.yml.example %{buildroot}%{_sysconfdir}/taskotron/trigger_rules.yml

install -d %{buildroot}%{_sysconfdir}/fedmsg.d/
install -p -m 0644 fedmsg.d/taskotron-trigger.py %{buildroot}%{_sysconfdir}/fedmsg.d/taskotron-trigger.py

install -d %{buildroot}%{_sysconfdir}/logrotate.d/
install -p -m 0644 conf/logrotate.d/taskotron-trigger %{buildroot}%{_sysconfdir}/logrotate.d/taskotron-trigger

install -d %{buildroot}%{_localstatedir}/log/taskotron-trigger/
install -d %{buildroot}%{_sharedstatedir}/taskotron-trigger/

%files
%doc readme.rst
%license LICENSE
%{python_sitelib}/*

%attr(755,root,root) %{_bindir}/jobrunner

%dir %attr(755,fedmsg,fedmsg) %{_localstatedir}/log/taskotron-trigger
%dir %attr(755,fedmsg,fedmsg) %{_sharedstatedir}/taskotron-trigger

%dir %{_sysconfdir}/taskotron
%{_sysconfdir}/fedmsg.d/taskotron-trigger.py*
%config(noreplace) %{_sysconfdir}/taskotron/trigger.cfg
%config(noreplace) %{_sysconfdir}/taskotron/trigger_rules.yml
%config(noreplace) %{_sysconfdir}/logrotate.d/taskotron-trigger

%changelog
* Mon Oct 31 2016 Tim Flink <tflink@fedoraproject.org> - 0.4.3-1
- Improve task discovery for dist-git (D1031)
- Add support for triggering on dist-git commit events (D1064)
- Use proper dist-git namespaces (D1062)
- Require mongoquery (D1068)

* Mon Oct 31 2016 Tim FLink <tflink@fedoraproject.org> - 0.4.2-1
- Adding more restrictions on the koji tags that can trigger tasks (D1042)

* Mon Oct 17 2016 Martin Krizek <mkrizek@redhat.com> - 0.4.1-2
- install trigger_rules.yml

* Thu Oct 13 2016 Martin Krizek <mkrizek@redhat.com> - 0.4.1-1
- remove mongoquery bundle

* Tue Oct 11 2016 Tim FLink <tflink@fedoraproject.org> - 0.4.0-1
- bumping version properly for a major release

* Tue Oct 11 2016 Tim FLink <tflink@fedoraproject.org> - 0.3.17-1
- rework trigger to be more easily configurable (D963)

* Fri Jul 22 2016 Martin Krizek <mkrizek@redhat.com> - 0.3.16-4
- remove rm -rf buildroot as it's not neccessary
- preserve timestamps of installed files

* Mon Jun 13 2016 Martin Krizek <mkrizek@redhat.com> - 0.3.16-3
- fix Source0 url
- fix conf files permissions

* Mon May 30 2016 Martin Krizek <mkrizek@redhat.com> - 0.3.16-2
- add license file
- add check
- fix url and source
- fix requires and buildrequires

* Wed May 25 2016 Martin Krizek <mkrizek@redhat.com> - 0.3.16-1
- Allow enabling distgit style tasks in config
- libabigail has been renamed to abicheck

* Fri May 6 2016 Martin Krizek <mkrizek@redhat.com> - 0.3.15-1
- Removing daemon as a dependency from requirements.txt (T768)
- dist-git support
- libabigail triggering

* Wed Mar 30 2016 Martin Krizek <mkrizek@redhat.com> - 0.3.14-1
- Add triggering for dockerautotest check

* Tue Jan 5 2016 Martin Krizek <mkrizek@redhat.com> - 0.3.13-2
- fix logrotate config file permissions

* Wed Dec 9 2015 Martin Krizek <mkrizek@redhat.com> - 0.3.13-1
- Use datagrepper to fetch jobs (D667)

* Mon May 25 2015 Martin Krizek <mkrizek@redhat.com> - 0.3.12-1
- fix blacklisting releases

* Wed Apr 8 2015 Martin Krizek <mkrizek@redhat.com> - 0.3.11-1
- fix triggering jobs when no task is configured

* Tue Mar 31 2015 Martin Krizek <mkrizek@redhat.com> - 0.3.10-1
- Add support for execdb

* Tue Sep 30 2014 Tim Flink <tflink@fedoraproject.org> - 0.3.9-1
- remove koji build queries to fix scheduling issues in T341

* Tue Sep 23 2014 Martin Krizek <mkrizek@redhat.com> - 0.3.8-1
- listen on primary koji instance only
- fix blacklisting releases of the form: '1.el7.1'

* Tue Sep 2 2014 Martin Krizek <mkrizek@redhat.com> - 0.3.7-1
- fix blacklisting releases in koji_tag consumer

* Thu Aug 21 2014 Martin Krizek <mkrizek@redhat.com> - 0.3.6-1
- list trigger.cfg as config file
- fix upgradepath scheduling

* Thu Aug 14 2014 Tim Flink <tflink@fedoraproject.org> - 0.3.5-1
- change to not schedule upgradepath on *-testing-pending

* Mon Jun 30 2014 Tim Flink <tflink@fedoraproject.org> - 0.3.3-1
- switching trigger over to use change_source instead of hacking at force build
- supporting x86_64-only checks

* Tue Jun 24 2014 Martin Krizek <mkrizek@redhat.com> - 0.3.2-1
- change koji tag triggering to use *-pending tags

* Mon Jun 23 2014 Martin Krizek <mkrizek@redhat.com> - 0.3.1-1
- add python-twisted as dep

* Mon Jun 16 2014 Tim Flink <tflink@fedoraproject.org> - 0.3.0-1
- releasing 0.3.0

* Fri Jun 13 2014 Tim Flink <tflink@fedoraproject.org> - 0.2.1-1
- support triggering with koji_tag on bodhi update change

* Thu May 29 2014 Tim Flink <tflink@fedoraproject.org> - 0.1.2-1
- Fixing typo that broke the jobrunner executable

* Fri May 23 2014 Martin Krizek <mkrizek@redhat.com> - 0.1.1-1
- Add jobrunner script
- Add logrotate conf file

* Fri May 16 2014 Tim Flink <tflink@fedoraproject.org> - 0.1.0-1
- Adding missing deps
- Releasing taskotron-trigger 0.1.0

* Tue Apr 15 2014 Tim Flink <tflink@fedoraproject.org> - 0.0.10-1
- Updating to latest upstream. Triggers buildbot builds based on item arch
- adding koji to requires

* Mon Mar 10 2014 Martin Krizek <mkrizek@fedoraproject.org> - 0.0.9-1
- Initial packaging
